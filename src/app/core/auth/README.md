# core.auth

> informações úteis

* [GitHub] github / soudev / knowledge.mine /

  * [... / angular](https://github.com/soudev/knowledge.mine/blob/master/stuff/angularjs.md)

    * ver item: **Autenticação**

--

URL do serviço de oauth ciss >> `http://oauth.ciss.com.br/api/{endpoint}`

```


valores enviados via POST num: x-www-form-urlencoded  ?

  identifica a aplicação que está solicitando o acesso na API

client_id=
client_secret=

  solicitar do usuário da aplicação

username=
password=
grant_type=password
```

--

https://www.base64encode.org/

--

```javascript
auth.init({
  endpoint: 'http://oauth.ciss.com.br/api/erp',
  clientId: '',
  clientSecrect: '',
  headers: {}
});
```

## Atividades Jazz - CISS

* [22342](https://jazz.ciss.com.br:9443/ccm/web/projects/ERPCloud#action=com.ibm.team.workitem.viewWorkItem&id=22342) - Estrutura para login

* [22152](https://jazz.ciss.com.br:9443/ccm/web/projects/ERPCloud#action=com.ibm.team.workitem.viewWorkItem&id=22152) - I18N - internacionalização

* [27126](https://jazz.ciss.com.br:9443/ccm/web/projects/ERPCloud#action=com.ibm.team.workitem.viewWorkItem&id=27126) - revisão do suporte e código de autenticação


## TODO

* pensar num suporte para adicionar no header o i18n

  * utilizar o atributo: `Accept-Language`

--

* Definir:

  * AuthService

    * signup >> serviço para criar um acesso


## Reler

* [Simple Authentication for Angular.js App | Alexander Beletsky](http://beletsky.net/2013/11/simple-authentication-in-angular-dot-js-app.html)

* [Set headers for all $http calls in AngularJS | Make It Easy](http://angulartutorial.blogspot.com.br/2014/05/set-headers-for-all-http-calls-in.html)

* [Techniques for authentication in AngularJS applications | Opinionated AngularJS — Medium](https://medium.com/opinionated-angularjs/techniques-for-authentication-in-angularjs-applications-7bbf0346acec)

--

* [[Gist] AngularJS service to send auth token with $resource requests](https://gist.github.com/nblumoe/3052052)

* [Posting Form Data With $http In AngularJS | Ben Nadel](http://www.bennadel.com/blog/2615-posting-form-data-with-http-in-angularjs.htm)

--

* [$http | Angular API Doc](https://docs.angularjs.org/api/ng/service/$http)

--

* [$q | Angular API Doc](https://docs.angularjs.org/api/ng/service/$q)

--

* [$q | Angularjs Docs](http://docs.angularjs.org/api/ng.$q)

  * [Promises in AngularJS, Explained as a Cartoon | Andy Shora](http://andyshora.com/promises-angularjs-explained-as-cartoon.html)

  * [A Simple Visual Model for Promises | Flippin' Awesome](http://flippinawesome.org/2013/10/14/a-simple-visual-model-for-promises/)

  * [Promises in AngularJS | Radu's blog](http://wooptoo.com/blog/promises-in-angularjs/)

  * [Understanding Promises in Angular | Bilal Quadri](http://bilalquadri.com/blog/2014/05/18/understanding-promises-in-angular/)

  * [AngularJS Promises - The Definitive Guide | dwmkerr](http://www.dwmkerr.com/promises-in-angularjs-the-definitive-guide/)

    * [AngularJS Promises - O guia definitivo | Nomadev](http://nomadev.com.br/angularjs-promises-promessas-o-guia-definitivo/)

  * [Exploring Angular 1.3: ES6 Style Promises | thoughtram](http://blog.thoughtram.io/angularjs/2014/12/18/exploring-angular-1.3-es6-style-promises.html)


## Interceptor Reponse Erro status code 0

* [Angular ISSUE 3336](https://github.com/angular/angular.js/issues/3336) - $http: unusable params passed to error handler when CORS preflight fails

> no backend revisar o retorno dos erros se este possuem o header do CORS


## Erros ao acessar oauth.ciss.com.br/api/central

* 20141218

```
XMLHttpRequest cannot load http://oauth.ciss.com.br/api/central.
No 'Access-Control-Allow-Origin' header is present on the requested resource.
Origin 'http://localhost:1337' is therefore not allowed access.
```

* 20141219

```
XMLHttpRequest cannot load http://oauth.ciss.com.br/api/central.
Request header field Content-Type is not allowed by Access-Control-Allow-Headers.

XMLHttpRequest cannot load http://oauth.ciss.com.br/api/central. 
Request header field clientCNPJ is not allowed by Access-Control-Allow-Headers.
```

  * [[StackOverflow] Error :Request header field Content-Type is not allowed by Access-Control-Allow-Headers](https://stackoverflow.com/questions/12409600/error-request-header-field-content-type-is-not-allowed-by-access-control-allow)

  * [CORS-Compliant REST API with Jersey and ContainerResponseFilter](http://blog.usul.org/cors-compliant-rest-api-with-jersey-and-containerresponsefilter/)
