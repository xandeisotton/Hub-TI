define(function(require) {
  'use strict';

  var module = require( '../module' );

  module.controller( 'LogoutCtrl', LogoutCtrl );

  //---

  LogoutCtrl.$inject = [ 'AuthService', 'BreadcrumbService' ];

  function LogoutCtrl( auth, BreadcrumbService ) {

    doLogout();

    BreadcrumbService.clear();

    //---

    function doLogout() {

      auth
        .logout( function(result) {

          console.log( 'AuthService' );
          console.log( 'isAuthenticated: ' + auth.isAuthenticated );
          console.log( auth.getToken() );

        });

    }

  }

});
