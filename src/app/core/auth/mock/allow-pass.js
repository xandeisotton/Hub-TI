define(function(require) {
  'use strict';

  var backend = require('shared/mock/backend');

  backend.addResource(AllowPass);

  //---

  // mock resource dependencies injection
  AllowPass.$inject = ['$httpBackend', 'regexpUrl'];

  // mock resource definition
  function AllowPass($httpBackend, regexpUrl) {

    $httpBackend.when('POST', regexpUrl(/\/\/oauth/)).passThrough();

  }

});
