define(function(require) {
  'use strict';

  var angular = require('angular');
  require('angularResource');
  require('angularTranslate');
  require('uiRouter');

  return angular.module('core.auth', [
    'pascalprecht.translate',
    'ngResource',
    'toaster',
    'ui.router'
  ]);

});
