define(function(require) {
  'use strict';

  var module = require('./module');

  require('./storage/token');

  require('./services/transformRequestAsFormPost');
  require('./services/http');
  require('./services/token');
  require('./services/page');
  require('./services/auth');

  require('./interceptors/auth');

  require('./controllers/signin');
  require('./controllers/signup');
  require('./controllers/logout');

  require('./states');
  require('./run');

  return module;

});
