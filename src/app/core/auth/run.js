define(function(require) {
  'use strict';

  var module = require('./module');

  module.run(runner);

  //---

  runner.$inject = [
    '$rootScope', '$location',
    'AuthPageService', 'AuthTokenService',
    'UserService'
  ];

  function runner( $rootScope, $location, pageService, tokenService, userService ) {

    function checkAuthState() {
      var currentPath = $location.path();
      var isLoginPage = ( currentPath === pageService.page.login );
      var isAuthorized = !!tokenService.getAccessToken();

      /*
      console.log( 'currentPath: ' + currentPath );
      console.log( 'isLoginPage: ' + isLoginPage );
      console.log( 'isAuthorized: ' + isAuthorized );
      */

      if( isLoginPage && isAuthorized ) {
        $location.path( pageService.page.home );
      } else if( !isAuthorized && !isLoginPage ) {

        // TODO: review
        userService.set( null );

        $location.path( pageService.page.login );
      }

    }

    // ngRoute
    /*
    $rootScope.$on( '$routeChangeStart', function (event, next, current) {

      console.log('\n\n$routeChangeStart\n\n');
      checkAuthState();

    });
    */

    // angular ui router
    $rootScope.$on(
      '$stateChangeStart',
      function (event, toState, toParams, fromState, fromParams) {

        // console.log('\n\n$stateChangeStart\n\n');
        checkAuthState();

      }
    );

  }

});
