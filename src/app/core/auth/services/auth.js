define(function(require) {
  'use strict';

  var module = require( '../module' );

  module.factory( 'AuthService', AuthService );

  //---

  AuthService.$inject = [
    '$q', '$location',
    'AuthTokenService', 'AuthHttpService', 'AuthPageService',
    'UserService'
  ];

  function AuthService( $q, $location, tokenService, httpService, pageService, userService ) {

    var config = {};

    //---

    var service = {
      init: doInit,
      signin: doSignin,
      signup: doSignup,
      signout: doLogout,
      logout: doLogout,
      getToken: tokenService.getAccessToken,
      goToHomePage: goToHomePage
    };

    return service;

    //---

    function doInit( options ) {

      /* options
      {
        endpoint: 'http://oauth.url/',
        clientId: '',
        clientSecrect: '',
        headers: {},
        page: {
          home: '/',
          login: '/signin'
        }
      }
      */

      if ( !options ) {
        throw new Error( 'You must set options when calling init' );
      }


      config.endpoint = options.endpoint;
      config.client_id = options.clientId || options.clientID || options.client_id;
      config.client_secret = options.clientSecret || options.client_secret;


      if( !config.endpoint ) {
        throw new Error( 'You must set options.endpoint when calling init' );
      }

      if( !config.client_id ) {
        throw new Error( 'You must set options.client_id when calling init' );
      }

      if( !config.client_secret ) {
        throw new Error( 'You must set options.client_secret when calling init' );
      }

      if( options.headers ) {
        config.headers = options.headers;
      }

      if( options.page ) {
        if(options.page.home) pageService.page.home = options.page.home;
        if(options.page.login) pageService.page.login = options.page.login;
      }

    }

    function doSignin( user, successHandler, errorHandler ) {
      checkAuthServiceInitialized();

      var credentials = {
        client_id       : config.client_id,
        client_secret   : config.client_secret,
        grant_type      : 'password',
        username        : null,
        password        : null
      };

      /* user
      {
        username: '',
        password: ''
      }
      */

      if( !user ) {
        throw new Error( 'You must set user info when calling signin' );
      }

      if( !user.username ) {
        throw new Error( 'You must set user.username when calling signin' );
      }
      credentials.username = user.username;

      if( !user.password ) {
        throw new Error( 'You must set user.password when calling signin' );
      }
      credentials.password = user.password;

      addLoginHeaders();

      return httpService.postForm(
        config.endpoint, credentials
      ).then(function postSuccessHandler( result ) {

        tokenService.set( result.data );

        // TODO: review
        userService.set({
          username: credentials.username
        });

        removeLoginHeaders();
        goToHomePage();

        if( isFunction( successHandler ) ) successHandler( result );

        return result;

      }, function postErrorHandler( error ) {

        removeLoginHeaders();

        if( isFunction( errorHandler ) ) errorHandler( error );

        return error;
      });

      //---

      function addLoginHeaders() {
        if(config.headers) httpService.headers.add( config.headers );
      }

      function removeLoginHeaders() {
        if(config.headers) httpService.headers.remove( config.headers );
      }

    }

    function doSignup( options, success, error ) {
      checkAuthServiceInitialized();

      throw new Error( 'not supported' );

    }

    function doLogout( cb ) {
      var promise = null;

      if( !cb ) {
        var deferred = Q.defer();
        cb = deferred.resolve;
        promise = deferred.promise;
      }

      tokenService.set( null );
      userService.set( null );

      (function goToLoginPage() {
        if( pageService.page && pageService.page.login )  {
          $location.path( pageService.page.login );
        }
      })();

      cb( 'done' );

      return promise;
    }

      //---

    function checkAuthServiceInitialized() {
      if(!config) throw new Error( 'You must initialize AuthService first' );
    }

    function goToHomePage() {
      if( pageService.page && pageService.page.home ) {
        $location.path( pageService.page.home );
      }
    }

  }

  //---

  function isFunction( object ) {
    return Object.prototype.toString.call( object ) == '[object Function]';
  }

});
