define(function(require) {
  'use strict';

  var angular   = require( 'angular' );
  var module    = require( '../module' );

  module.factory( 'AuthHttpService', AuthHttpService );

  //---

  AuthHttpService.$inject = [
    '$http', '$q', 'AuthTransformRequestAsFormPost'
  ];

  function AuthHttpService( $http, $q, transformRequestAsFormPost ) {

    var service = {
      headers: {
        get     : getHeader,
        add     : addHeader,
        remove  : removeHeader
      },
      postForm  : postForm
    };

    return service;

    //---
    // @begin: headers

    function addHeader() {
      switch( arguments.length ) {
        case 1:
          oneArgument( arguments[0] );
          break;
        case 2:
          twoArguments( arguments[0], arguments[1] );
          break;
        default:
          invalidArguments();
      }

      function oneArgument( object ) {
        if( !angular.isObject( object ) ) invalidArguments( 'Invalid argument : must be an object' );
        for(var key in object) {
          twoArguments( key, object[key] );
        }
      }

      function twoArguments( key, value ) {
        if( !angular.isString( key ) )  invalidArguments( 'Invalid argument : key must be a string' );
        $http.defaults.headers.common[ key ] = value;
      }

      function invalidArguments( message ) {
        var showMessage = 'Invalid arguments';
        if( message ) showMessage = message;
        throw new Error( showMessage );
      }
    }

    function removeHeader( toRemove ) {

      if( angular.isString( toRemove ) ) {
        removeKey( toRemove );
      } else if( angular.isObject( toRemove ) ) {
        for( var name in toRemove ) {
          if( !toRemove.hasOwnProperty( name ) ) continue;
          removeKey( name );
        }
      }

      function removeKey( key ) {
        if( $http.defaults.headers.common[ key ] ) {
          delete $http.defaults.headers.common[ key ];
        }
      }
    }

    function getHeader( key ) {
      var value = null;

      if( $http.defaults.headers.common[ key ] ) {
        value = $http.defaults.headers.common[ key ];
      }

      return value;
    }

    // @end: headers
    //---

    function postForm( url, data ) {

      return $q(function( resolve, reject ) {
        $http({
          method            : 'post',
          url               : url,
          transformRequest  : transformRequestAsFormPost,
          data              : data
        })
        .success(function successHandler( data, status, headers, config ) {
          resolve({
            data    : data,
            status  : status
          });
        })
        .error(function errorHandler( data, status, headers, config ) {
          reject({
            data    : data,
            status  : status
          });
        });
      });

    }

  }

});
