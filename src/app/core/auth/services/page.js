define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('AuthPageService', AuthPageService);

  //---

  // AuthPageService.$inject = [ ];

  function AuthPageService() {

    var service = {
      page: {
        home: '/',
        login: '/signin'
      }
    };

    return service;

  }


});
