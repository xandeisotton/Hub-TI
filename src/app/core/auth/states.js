define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  // https://github.com/angular-ui/ui-router/wiki

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    // TODO: review
    /*
    $urlRouterProvider
      .when('/auth', '/auth/login'); // default
    */

    $stateProvider

      .state('signin', {
        url: '/signin',
        views: {
          'master': {
            templateUrl   : 'app/core/auth/templates/signin.html',
            controller    : 'SigninCtrl',
            controllerAs  : 'signin'
          }
        }
      })

      .state('signup', {
        url: '/signup',
        views: {
          'master': {
            templateUrl   : 'app/core/auth/templates/signup.html',
            controller    : 'SignupCtrl',
            controllerAs  : 'signup'
          }
        }
      })

      .state('logout', {
        url: '/logout',
        views: {
          'master': {
            controller    : 'LogoutCtrl'
          }
        }
      });

  }

});
