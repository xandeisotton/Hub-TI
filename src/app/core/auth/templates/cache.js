define(['angular'], function() {

  /*
    Do not change this file, it will be updated from html2js >> in some future

    Não altere esse arquivo, ele será atualizado pelo html2js >> futuramente
  */

  angular.module('core.auth.cache', []);

  return { name: 'core.auth.cache' };

});
