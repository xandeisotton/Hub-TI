describe('Entrar:', function() {

  var vm, auth, location, httpBackend, controller;

  // executa antes de cada "it"
  beforeEach(function() {

    // carrega o módulo
    module('core.auth');

    // injeta as dependencias
    inject(function(AuthService, $location, $controller, $httpBackend) {

      vm = $controller('SigninCtrl');

      auth = AuthService;

      auth.init({
        endpoint        : 'http://oauth.ciss.com.br/api/central',
        client_id       : 'cisscentral',
        client_secret   : 'rest@38472@central!w9wi4xmd!',
        headers: {
          'clientCNPJ': 19489530000254
        }
      });

      location = $location;
      httpBackend = $httpBackend;
      controller = $controller;
    });

    // intercepta o endpoint
    httpBackend.when('POST', 'http://oauth.ciss.com.br/api/central')
      .respond(function(method, url, data) {
        if( ~data.indexOf('19489530000254') ){
          data = {
            access_token: 34234234111320203,
            expires_in: 3600
          };
          return [200, data];
        }
        return [400, null];
      });

    // seta a página de login
    location.path('/signin');

  });

  it('SigninCtrl deve ser definido', function() {
    expect(vm).toBeDefined();
  });

  it('AuthService deve ser definido', function() {
    expect(auth).toBeDefined();
  });

  it('deve não estar autenticado', function() {
    expect(auth.isAuthenticated).toEqual(false);
  });

  describe('Sucesso:', function() {

    beforeEach(function() {
      vm.username = '19489530000254';
      vm.password = '19489530000254';

      spyOn(location, 'path');

      vm.doLogin();

      httpBackend.flush();
    });

    it('deve estar autenticado', function() {
      expect(auth.isAuthenticated).toEqual(true);
    });

    it('o token deve estar definido', function() {
      expect(auth.getToken()).toEqual(34234234111320203);
    });

    it('deve ir para a página "home"', function() {
      expect(location.path).toHaveBeenCalledWith('/home');
    });

    it('ao acessar "signin" deve ir para "home"', function() {
      controller('SigninCtrl');
      expect(location.path).toHaveBeenCalledWith('/home');
    });

  });

  describe('Erro:', function() {

    beforeEach(function() {
      vm.username = '194895300';
      vm.password = '194895300';

      vm.doLogin();

      httpBackend.flush();
    });

    it('não deve estar autenticado', function() {
      expect(auth.isAuthenticated).toEqual(false);
    });

    it('o token não deve estar definido', function() {
      expect(auth.getToken()).toEqual(null);
    });

    it('deve permanecer na página "signin"', function() {
      expect(location.path()).toEqual('/signin');
    });

  });

});
