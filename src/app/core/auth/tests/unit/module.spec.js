describe('Angular.js \'core.auth\' Module', function() {

  var module;

  // executa antes de cada "it"
  beforeEach(function() {

    // carrega o módulo
    module = angular.module('core.auth');

  });

  it('deve ser definido', function() {

    // assertions
    expect(module).toBeDefined();

  });

  describe('Dependencias:', function() {

    var deps;

    var hasModule = function(m) {
      return deps.indexOf(m) >= 0;
    };

    beforeEach(function() {
      deps = module.value('appName').requires;
    });

    var mainDeps = [
      'ngResource',

      'toaster',

      'ui.router'
    ];

    mainDeps.forEach(function( depName ) {

      it('deve ter ' + depName +  ' como uma dependência', function() {

        // assertions
        expect(hasModule( depName )).toEqual(true);

      });

    });

  });

});
