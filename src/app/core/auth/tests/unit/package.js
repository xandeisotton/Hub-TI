define(function(require) {
  'use strict';

  require('./module.spec');
  require('./controllers/logout.spec');
  require('./controllers/signin.spec');
  require('./states.spec');

});
