describe('ui.router: \'core.auth\'', function() {

  var state;

  // executa antes de cada 'it'
  beforeEach(function() {

    // carrega o módulo
    module('core.auth');

    // injeta as dependencias
    inject(function($state) {
      state = $state;
    });

  });

  describe('Mapear States:', function() {

    it('$state deve ser definido', function() {
      expect(state).toBeDefined();
    });

    describe('Signin state:', function() {

      var config;

      it('deve ser definido', function() {
        // arrange
        config = state.get('signin');

        // assertions
        expect(config).toBeDefined();
      });

      it('deve mapear a url \'/signin\'', function() {
        expect(config.url).toEqual('/signin');
      });

      describe('Views:', function() {

        var views;

        it('deve ser definido', function() {
          // arrange
          views = config.views;

          // assertions
          expect(views).toBeDefined();
        });

        describe('master:', function() {

          var master;

          it('deve ser definido', function() {
            // arrange
            /*jshint sub:true*/
            master = views['master'];

            // assertions
            expect(master).toBeDefined();
          });

          it('deve mapear o template \'app/core/auth/templates/signin.html\'', function() {
            expect(master.templateUrl).toEqual('app/core/auth/templates/signin.html');
          });

        });
      });
    });
  });


});
