define(function(require) {
  'use strict';

  var module = require('./module');
  require('./service/crud');
  require('./service/search');

  return module;

});
