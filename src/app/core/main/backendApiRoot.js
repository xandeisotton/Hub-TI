define(function(require) {
  'use strict';

  var module = require('./module');

  //module.value('BACKEND_API_URL_ROOT', 'http://172.20.0.22:8080/cissgrm/api');
  module.value('BACKEND_API_URL_ROOT', 'http://127.0.0.1:8080/cissgrm/api');

});
