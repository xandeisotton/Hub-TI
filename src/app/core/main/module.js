define(function(require) {
  'use strict';

  var angular = require('angular');

  require('uiRouter');
  require('uiBootstrap');
  require('angularAria');
  require('angularMaterial');

  // angular module definition
  return angular.module(

    // module name
    'main',

    // module dependencies
    [
      'ui.router',
      'ui.bootstrap',
      'ngAria',
      'ngMaterial',

      require('./templates/cache').name,

      require('shared/fend/progressbar-loading/package').name,
      require('shared/fend/navbar/package').name


      // TODO: add here module to load


    ]
  );

});
