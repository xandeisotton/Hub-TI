define(function(require) {
  'use strict';

  var module = require('./module');
  require('./controller');
  require('./backendApiRoot');
  require('./states');

  return module;

});
