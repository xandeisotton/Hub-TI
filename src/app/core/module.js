define(function(require) {
  'use strict';

  var angular = require('angular');

  // angular module definition
  return angular.module(
    // module name
    'core',

    // module dependencies
    [

      require('./home/package').name,
      require('./about/package').name,

      require('./help/package').name,

      require('./main/package').name,
      require('./generic/package').name,

      // TODO: remover (temporário)
      require('app/modules/package').name

    ]
  );

});
