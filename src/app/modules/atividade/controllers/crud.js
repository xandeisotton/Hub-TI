define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('AtividadeCrudCtrl', AtividadeCrudCtrl);

  AtividadeCrudCtrl.$inject = ['AtividadeResource', '$state', 'GenericCrudService' ];


  function AtividadeCrudCtrl(Resource, $state, GenericCrudService ) {

    var vm = this;

    var path = 'atividade';

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Atividade';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Atividade';

    //Título do formulário
    vm.title = 'Atividade';

    GenericCrudService.init( vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idAtividade', onInit );

    function onInit() {

    }

  }

});
