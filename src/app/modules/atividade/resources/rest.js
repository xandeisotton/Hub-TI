define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('AtividadeResource', AtividadeResource);

  //---

  AtividadeResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function AtividadeResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/atividade/:id', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            method : 'POST',
            url : rootUrl + '/atividade/:subpath',
            params : {subpath : 'findwhen'}
        }
      }
    );

    return rest;

  }

});
