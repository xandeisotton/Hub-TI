define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/atividade', '/atividade/list'); // default

    $stateProvider
      .state('atividade', {
        abstract: true,
        url: '/atividade',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('atividade.list', {
        url: '/list',
        views: {
          'content@atividade': {
            templateUrl   : 'app/modules/atividade/templates/list.html',
            controller    : 'AtividadeSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('atividade.new', {
        url: '/new',
        views: {
          'content@atividade': {
            templateUrl   : 'app/modules/atividade/templates/form.html',
            controller    : 'AtividadeCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('atividade.edit', {
        url: '/edit/:id',
        views: {
          'content@atividade': {
            templateUrl   : 'app/modules/atividade/templates/form.html',
            controller    : 'AtividadeCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
