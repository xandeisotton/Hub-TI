define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('AtividadeEmpresaSearchCtrl', AtividadeEmpresaSearchCtrl);

  AtividadeEmpresaSearchCtrl.$inject = [
    'GenericViewService', 'AtividadeEmpresaResource', '$location', '$filter', 'PaginationFactory', 'NotificacaoResource' ];

    /**
    * @author Talysson de Castro<talysson.castro@ciss.com.br>
    * @date 14/07/2015
    * @version 1.0
    */
  function AtividadeEmpresaSearchCtrl( GenericView, Resource, $location, $filter, pagination, NotificacaoResource ) {
      var vm = this;

      pagination = pagination.get('AtividadeEmpresaSearchCtrl');

      //vm.order = orderFn;
      vm.onEditClick = onEditClickFn;

      vm.editarAtividade = editarAtividadeFn;

      vm.registroAtividade = [];

      // Inicializa o generic: contexto, resource, modulo, id do objeto.
      GenericView.init(vm, Resource, 'atividadeempresa', 'idAtividadeEmpresa');

      //carrega o conteúdo do grid
      vm.initPagination();

      //Ação do editar
      function onEditClickFn(data) {
        $location.path('atividade_empresa/edit/' + data.idObjeto);
      }

      // Ordena a lista do grid.
      function orderFn(data) {
        vm.provider = $filter('orderBy')(vm.provider, data.column, !data.desc);
      }

      (function listaTarefas() {
        Resource.findAtividade({id: 1}, function (result) {
          vm.registroAtividade = result;
        });
      })();

      function editarAtividadeFn(registroAtividade){
        NotificacaoResource.update(registroAtividade);
      }


  }

});
