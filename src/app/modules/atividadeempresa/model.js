define(function() {
  'use strict';

  return {

    'idAtividadeEmpresa':undefined,
    'atividade':{
      'idAtividade':undefined,
      'tpPeriodicidade':undefined,
      'dsDescricao':undefined,
      'tpTipoRealizacao':undefined
    },
    'empresa':{
      'idEmpresa':undefined,
      'dsRazaoSocial':undefined,
      'dsNomeFantasia':undefined,
      'dsResponsavelLegal':undefined,
      'dsCpf':undefined,
      'dsCnpj':undefined,
      'dsNire':undefined,
      'fgStatus':undefined,
      'endereco':undefined
    }
  };

});
