define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('AtividadeEmpresaResource', AtividadeEmpresaResource);

  //---

  AtividadeEmpresaResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function AtividadeEmpresaResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/atividadeempresa/:id', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            'method' : 'POST',
            url : rootUrl + '/atividadeempresa/:subpath',
            params : {subpath : 'findwhen'}
        },
        'findPaginationAtividade':{
            'method': 'GET', isArray:true,
            url:rootUrl + '/atividade'
        },
        'findPaginationEmpresa':{
            'method': 'GET', isArray:true,
            url:rootUrl + '/empresa'
        },
        'findAtividade': {
          'method': 'GET', isArray: true,
          url: rootUrl + '/notificacao/atividade/:id'
        },
        'editarAtividade':{
          'method': 'PUT',
          url:rootUrl + 'notificacao/updateatividade'
        }
      }
    );

    return rest;

  }

});
