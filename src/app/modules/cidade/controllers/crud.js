define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('CidadeCrudCtrl', CidadeCrudCtrl);

  CidadeCrudCtrl.$inject = ['CidadeResource', '$state', 'GenericCrudService' ];

  function CidadeCrudCtrl(Resource, $state, GenericCrudService ) {

    var vm = this;

    var path = 'cidade';

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Cidade';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Cidade';

    //Título do formulário
    vm.title = 'Cidade';

    vm.estado = [];

    GenericCrudService.init( vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idCidade', onInit );

    function onInit() {

    }

    function selectEstado(){
      Resource.findPagination(function(result){
        vm.estado = result;
      });
    }

    selectEstado();

    }

});
