define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('CidadeSearchCtrl', CidadeSearchCtrl);

  CidadeSearchCtrl.$inject = [
    'GenericViewService', 'CidadeResource', '$location', '$filter', 'PaginationFactory' ];

  function CidadeSearchCtrl( GenericView, Resource, $location, $filter, pagination ) {
    var vm = this;

    pagination = pagination.get('CidadeSearchCtrl');

    //vm.order = orderFn;
    vm.onEditClick = onEditClickFn;

    // Inicializa o generic: contexto, resource, modulo, id do objeto.
    GenericView.init( vm, Resource, 'cidade', 'idCidade' );

    //carrega o conteúdo do grid
    vm.initPagination();

    vm.iniciarEdicao = iniciarEdicaoFn;

    vm.cancelarEdicao = cancelarEdicaoFn;

    //Ação do editar
    function onEditClickFn(data) {
      $location.path('cidade/edit/' + data.idObjeto);
    }

    // Ordena a lista do grid.
    function orderFn(data) {
      vm.provider = $filter('orderBy')(vm.provider, data.column, !data.desc);
    }

    function onSaveClickFn(data){
      Resource.save(data, function(result){

        vm.item = generateNewItemFn();

        vm.initPagination();
      });
    }

    function generateNewItemFn() {
      var model = require('../model');
      return new Resource( angular.copy( model ) );
    }

    function iniciarEdicaoFn (data) {
      vm.dsCidadeOriginal = data.dsCidade;
      vm.dsCepOriginal = data.dsCep;
      data.edit = true;
    }

    function cancelarEdicaoFn (data) {
      data.dsCidade = vm.dsCidadeOriginal;
      data.dsCep = vm.dsCep;
      //data = vm.objetoCopy;
      data.edit = false;
    }

  }

});
