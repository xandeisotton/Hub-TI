define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/cidade', '/cidade/list'); // default

    $stateProvider
      .state('cidade', {
        abstract: true,
        url: '/cidade',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('cidade.list', {
        url: '/list',
        views: {
          'content@cidade': {
            templateUrl   : 'app/modules/cidade/templates/list.html',
            controller    : 'CidadeSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('cidade.new', {
        url: '/new',
        views: {
          'content@cidade': {
            templateUrl   : 'app/modules/cidade/templates/form.html',
            controller    : 'CidadeCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('cidade.edit', {
        url: '/edit/:id',
        views: {
          'content@cidade': {
            templateUrl   : 'app/modules/cidade/templates/form.html',
            controller    : 'CidadeCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
