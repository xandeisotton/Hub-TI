define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('ContatoCrudCtrl', ContatoCrudCtrl);

  ContatoCrudCtrl.$inject = ['ContatoResource', '$state', 'GenericCrudService' ];

  /**
  * @author Luiz Gustavo Schneider<gustavofblack12@gmail.com>
  * @date 03/09/2015
  * @version 1.0
  * */
  function ContatoCrudCtrl(Resource, $state, GenericCrudService ) {

    var vm = this;

    var path = 'contato';

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Contato';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Contato';

    //Título do formulário
    vm.title = 'Contato';

    vm.empresas = [];

    GenericCrudService.init( vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idContato', onInit );

    function onInit() {

    }

    function selectEmpresa(){
      Resource.findPaginationEmpresa(function(result){
        vm.empresas = result;
      });
    }

  selectEmpresa();


  }
});
