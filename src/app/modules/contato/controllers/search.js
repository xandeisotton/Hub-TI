define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('ContatoSearchCtrl', ContatoSearchCtrl);

  ContatoSearchCtrl.$inject = [
    'GenericViewService', 'ContatoResource', '$location', '$filter', 'PaginationFactory', '$mdDialog'];

    /**
    * @author Luiz Gustavo Schneider<gustavofblack12@gmail.com>
    * @date 03/09/2015
    * @version 1.0
    */
  function ContatoSearchCtrl( GenericView, Resource, $location, $filter, pagination, $mdDialog ) {
    var vm = this;

    pagination = pagination.get('ContatoSearchCtrl');

    //vm.order = orderFn;
    vm.onEditClick = onEditClickFn;

    vm.showConfirm = showConfirmFn;

    // Inicializa o generic: contexto, resource, modulo, id do objeto.
    GenericView.init( vm, Resource, 'contato', 'idContato' );

    //carrega o conteúdo do grid
    vm.initPagination();

    //Ação do editar
    function onEditClickFn(data) {
      $location.path('contato/edit/' + data.idObjeto);
    }

    // Ordena a lista do grid.
    function orderFn(data) {
      vm.provider = $filter('orderBy')(vm.provider, data.column, !data.desc);
    }

    function showAlert(ev) {
      $mdDialog.show({
        //controller: DialogController,
        templateUrl: 'app/modules/contato/templates/form.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true
      });
    }

      function showConfirmFn(ev, contato) {
        var confirm = $mdDialog.confirm()
            .title('Excluir')
            .content('Voce realmente deseja excluir este registro')
            .ariaLabel('Lucky day')
            .clickOutsideToClose(true)
            .targetEvent(ev)
            .ok('OK')
            .cancel('Cancelar');
        $mdDialog.show(confirm).then(function() {
          vm.onDeleteClick(contato);
        }, function() {
        });
      }

  }

});
