define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('ControleInternoCrudCtrl', ControleInternoCrudCtrl);

  ControleInternoCrudCtrl.$inject = ['ControleInternoResource', '$state', 'GenericCrudService' ];

  /**
  * @author Luiz Gustavo Schneider<gustavofblack12@gmail.com>
  * @date 03/08/2015
  * @version 1.0
  * */
  function ControleInternoCrudCtrl(Resource, $state, GenericCrudService ) {

    var vm = this;

    var path = 'controleInterno';

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Controle Interno';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Controle Interno';

    //Título do formulário
    vm.title = 'Controles Internos';

    vm.empresas = [];

    GenericCrudService.init( vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idControleInterno', onInit );

    function onInit() {

    }

    function selectEmpresa(){
      Resource.findPagination(function(result){
        vm.empresas= result;
    });
  }

  selectEmpresa();

  }
});
