define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('ControleInternoResource', ControleInternoResource);

  //---

  ControleInternoResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function ControleInternoResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/controle_interno/:id', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            method : 'POST',
            url : rootUrl + '/controle_interno/:subpath',
            params : {subpath : 'findwhen'}
        },

        'findPagination':{
          method: 'GET', isArray:true,
          url:rootUrl + '/empresa'
        }

      }
    );

    return rest;

  }

});
