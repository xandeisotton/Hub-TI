define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/controleInterno', '/controleInterno/list'); // default

    $stateProvider
      .state('controleInterno', {
        abstract: true,
        url: '/controleInterno',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('controleInterno.list', {
        url: '/list',
        views: {
          'content@controleInterno': {
            templateUrl   : 'app/modules/controleInterno/templates/list.html',
            controller    : 'ControleInternoSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('controleInterno.new', {
        url: '/new',
        views: {
          'content@controleInterno': {
            templateUrl   : 'app/modules/controleInterno/templates/form.html',
            controller    : 'ControleInternoCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('controleInterno.edit', {
        url: '/edit/:id',
        views: {
          'content@controleInterno': {
            templateUrl   : 'app/modules/controleInterno/templates/form.html',
            controller    : 'ControleInternoCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
