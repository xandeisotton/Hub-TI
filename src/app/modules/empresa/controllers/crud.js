define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('EmpresaCrudCtrl', EmpresaCrudCtrl);

  EmpresaCrudCtrl.$inject = ['EmpresaResource', '$state', 'GenericCrudService', '$mdDialog'];


  function EmpresaCrudCtrl(Resource, $state, GenericCrudService, $mdDialog) {

    var vm = this;

    var path = 'empresa';

    vm.atividades = [];

    vm.cidades = [];

    vm.enderecos = [];

    vm.addFilial = addFilialFn;
    vm.addSocio = addSocioFn;
    vm.addRegularidadeFiscal = addRegularidadeFiscalFn;
    vm.addParecer = addParecerFn;
    vm.addContato = addContatoFn;
    vm.addControleInternoFn = addControleInternoFn;

    vm.clearFilial = clearFilialFn;
    vm.clearSocio = clearSocioFn;
    vm.clearRegularidadeFiscal = clearRegularidadeFiscalFn;
    vm.clearParecer = clearParecerFn;
    vm.clearContato = clearContatoFn;
    vm.clearControleInterno = clearControleInternoFn;

    vm.changeAtividade = changeAtividade;

    vm.searchTextCidade = searchTextCidadeFn;

    vm.querySearchCidade = querySearchCidadeFn;

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Empresa';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Empresa';

    vm.close = closeFn;

    //Título do formulário
    vm.title = 'Empresa';

    vm.showAlertFilial = showAlertFilial;
    vm.showAlertSocio = showAlertSocio;
    vm.showAlertRegFiscal = showAlertRegFiscal;
    vm.showAlertParecer = showAlertParecer;
    vm.showAlertContato = showAlertContato;
    vm.showAlertControle = showAlertControle;

    GenericCrudService.init( vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idEmpresa', onInit );

    function onInit() {

    }

    function querySearchCidadeFn (query) {
      var results = query ? vm.cidades.filter( createFilterForCidadeFn(query) ) : vm.cidades,
          deferred;
      if (self.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }

    function selectCidadeFn(){
      vm.cidades = [];
      Resource.findPaginationEndereco(function(result){
        vm.cidades = result;
        console.log(vm.cidades);
      });
    }

    selectCidadeFn();

    function createFilterForCidadeFn(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(cidade) {
        return (angular.lowercase(endereco.cidade.dsCep).indexOf(lowercaseQuery) === 0);
      };
    }

    function searchTextCidadeFn(text){

    }

    function selectAtividade(){
      Resource.findPaginationAtividade(function(result){
        vm.atividades = result;
      });
    }

    selectAtividade();

    function closeFn(){
      $mdDialog.hide();
    }

    function showAlertFilial(ev) {
      $mdDialog.show({
        controller: EmpresaCrudCtrl,
        controllerAs: 'vm',
        templateUrl: 'app/modules/filial/templates/form.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true
      })
      .then(function(item) {
        Resource.save(item, function() {
          vm.item.listaFilial.push(item);
        });
      });
    }

    function showAlertSocio(ev) {
      $mdDialog.show({
        controller: EmpresaCrudCtrl,
        controllerAs: 'vm',
        templateUrl: 'app/modules/socio/templates/form.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true
      })
      .then(function(item) {
        Resource.save(item, function() {
          vm.item.listaSocio.push(item);
        });
      });
    }

    function showAlertRegFiscal(ev) {
      $mdDialog.show({
        controller: EmpresaCrudCtrl,
        controllerAs: 'vm',
        templateUrl: 'app/modules/regularidadefiscal/templates/form.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true
      })
      .then(function(item) {
        Resource.save(item, function() {
          vm.item.listaRegularidadeFiscal.push(item);
        });
      });
    }

    function showAlertParecer(ev) {
      $mdDialog.show({
        controller: EmpresaCrudCtrl,
        controllerAs: 'vm',
        templateUrl: 'app/modules/parecer/templates/form.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true
      })
      .then(function(item) {
        Resource.save(item, function() {
          vm.item.listaParecer.push(item);
        });
      });
    }

    function showAlertContato(ev) {
      $mdDialog.show({
        controller: EmpresaCrudCtrl,
        controllerAs: 'vm',
        templateUrl: 'app/modules/contato/templates/form.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true
      })
      .then(function(item) {
        Resource.save(item, function() {
          vm.item.listaContato.push(item);
        });
      });
    }

    function showAlertControle(ev) {
      $mdDialog.show({
        controller: EmpresaCrudCtrl,
        controllerAs: 'vm',
        templateUrl: 'app/modules/controleInterno/templates/form.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true
      })
      .then(function(item) {
        Resource.save(item, function() {
          vm.item.listaControleInterno.push(item);
        });
      });
    }

  function changeAtividade(data){
    if(data.fgSelecionado){
      addAtividade(data);
    }else{
      removeAtividade(data);
    }
  }

  function addAtividade(atividade){
    vm.item.listaAtividadeEmpresa.push(angular.copy(atividade));
    delete vm.item.listaAtividadeEmpresa[vm.item.listaAtividadeEmpresa.length -1].fgSelecionado;
  }

  function removeAtividade(atividade){
    for (var i = 0; i <= vm.item.listaAtividadeEmpresa.length; i++) {
      if(atividade === vm.item.listaAtividadeEmpresa[i]){
        vm.item.listaAtividadeEmpresa.splice(i,1);
      }
    }
  }

  function addFilialFn(data){
    var filial = {
      dsRazaoSocial: data.dsRazaoSocial,
      dsCnpj : data.dsCnpj
    };
    vm.item.listaFilial.push(angular.copy(filial));
  }

  function addSocioFn(data){
    var socio = {
      dsSocio : data.dsSocio,
      vlCapital : data.vlCapital,
      dsAdministrador : data.dsAdministrador
    };
    vm.item.listaSocio.push(angular.copy(socio));
  }

  function addRegularidadeFiscalFn(data){
    var regularidadefiscal = {
      fgTipo : data.fgTipo,
      fgStatus : data.fgStatus,
      dsObservacao : data.dsObservacao
    };
    vm.item.listaRegularidadeFiscal.push(angular.copy(regularidadefiscal));
  }

  function addParecerFn(data){
    var parecer = {
      dsTitulo : data.dsTitulo,
      dsDescricao : data.dsDescricao
    };
    vm.item.listaParecer.push(angular.copy(parecer));
  }

  function addContatoFn(data){
    var contato = {
      dsNome : data.dsNome,
      dsTelefone : data.dsTelefone,
      dsEmail : data.dsEmail,
      dsChat : data.dsChat
    };
    vm.item.listaContato.push(angular.copy(contato));
  }

  function addControleInternoFn(data){
    var controleInterno = {
      dsControle : data.dsControle,
      dsTelefone : data.dsTelefone
    };
    vm.item.listaControleInterno.push(angular.copy(controleInterno));
  }

  function PopupController($scope, $mdDialog) {
    $scope.cancel = function() {
      $mdDialog.cancel();
    };
    $scope.save = function(item) {
      $mdDialog.hide(item);
    };
  }

  function clearFilialFn(data){
    data.dsRazaoSocial = undefined;
    data.dsCnpj = undefined;
  }

  function clearSocioFn(data){
    data.dsSocio = undefined;
    data.vlCapital = undefined;
    data.dsAdministrador = undefined;
  }

  function clearRegularidadeFiscalFn(data){
    data.fgTipo = undefined;
    data.fgStatus = undefined;
    data.dsObservacao = undefined;
  }

  function clearParecerFn(data){
    data.dsTitulo = undefined;
    data.dsDescricao = undefined;
  }

  function clearControleInternoFn(data){
    data.dsControle = undefined;
    data.dsNome = undefined;
    data.dsTelefone = undefined;
  }

  function clearContatoFn(data){
    data.dsNome = undefined;
    data.dsEmail = undefined;
    data.dsTelefone = undefined;
    data.dsChat = undefined;
  }
}

});
