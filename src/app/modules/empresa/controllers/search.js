define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('EmpresaSearchCtrl', EmpresaSearchCtrl);

  EmpresaSearchCtrl.$inject = [
    'GenericViewService', 'EmpresaResource', '$location', '$filter', 'PaginationFactory'];

    /**
    * @author Talysson de Castro<talysson.castro@ciss.com.br>
    * @date 14/07/2015
    * @version 1.0
    */
  function EmpresaSearchCtrl( GenericView, Resource, $location, $filter, pagination){
    var vm = this;

    pagination = pagination.get('EmpresaSearchCtrl');

    //vm.order = orderFn;
    vm.onEditClick = onEditClickFn;

    // Inicializa o generic: contexto, resource, modulo, id do objeto.
    GenericView.init( vm, Resource, 'empresa', 'idEmpresa');

    //carrega o conteúdo do grid
    vm.initPagination();

    //Ação do editar
    function onEditClickFn(data) {
      $location.path('empresa/edit/' + data.idObjeto);
    }

    // Ordena a lista do grid.
    function orderFn(data) {
      vm.provider = $filter('orderBy')(vm.provider, data.column, !data.desc);
    }

  }
});
