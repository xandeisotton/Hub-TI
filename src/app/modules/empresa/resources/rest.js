define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('EmpresaResource', EmpresaResource);

  //---

  EmpresaResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function EmpresaResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/empresa/:id', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            method : 'POST',
            url : rootUrl + '/empresa/:subpath',
            params : {subpath : 'findwhen'}
        },
        'findPaginationEndereco':{
          'method' : 'GET' , isArray: true,
          url : rootUrl + '/endereco'
        },
        'findPaginationCidade':{
          'method' : 'GET' , isArray: true,
          url : rootUrl + '/cidade'
        },
        'findPaginationAtividade':{
          'method' : 'GET' , isArray: true,
          url : rootUrl + '/atividade'
        }
      }
    );

    return rest;

  }

});
