define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/empresa', '/empresa/list'); // default

    $stateProvider
      .state('empresa', {
        abstract: true,
        url: '/empresa',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('empresa.list', {
        url: '/list',
        views: {
          'content@empresa': {
            templateUrl   : 'app/modules/empresa/templates/list.html',
            controller    : 'EmpresaSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('empresa.new', {
        url: '/new',
        views: {
          'content@empresa': {
            templateUrl   : 'app/modules/empresa/templates/form.html',
            controller    : 'EmpresaCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('empresa.edit', {
        url: '/edit/:id',
        views: {
          'content@empresa': {
            templateUrl   : 'app/modules/empresa/templates/form.html',
            controller    : 'EmpresaCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
