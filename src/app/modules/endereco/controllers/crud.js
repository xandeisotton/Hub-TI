define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('EnderecoCrudCtrl', EnderecoCrudCtrl);

  EnderecoCrudCtrl.$inject = ['EnderecoResource', '$state', 'GenericCrudService' ];

  /**
  * @author Luiz Gustavo Schneider<gustavofblack12@gmail.com>
  * @date 29/08/2015
  * @version 1.0
  * */
  function EnderecoCrudCtrl(Resource, $state, GenericCrudService ) {

    var vm = this;

    var path = 'endereco';

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Endereco';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Endereco';

    //Título do formulário
    vm.title = 'Endereco';

    GenericCrudService.init( vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idEndereco', onInit );

    function onInit() {

    }

    function selectCidade(){
      vm.cidades=[];
      Resource.findPaginationCidade(function(result){
        vm.cidades = result;
      });
    }

    function selectEstado(){
      vm.estados=[];
      Resource.findPaginationEstado(function(result){
        vm.estados = result;
    });
  }

      function selectCep(){
      vm.dsCep=[];
      Resource.findPaginationCidade(function(result){
        vm.dsCep = result;
      });
    }

  selectEstado();
  selectCidade();

  }
});
