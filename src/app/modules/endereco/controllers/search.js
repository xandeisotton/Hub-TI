define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('EnderecoSearchCtrl', EnderecoSearchCtrl);

  EnderecoSearchCtrl.$inject = [
    'GenericViewService', 'EnderecoResource', '$location', '$filter', 'PaginationFactory', '$mdDialog' ];

    /**
    * @author Talysson de Castro<talysson.castro@ciss.com.br>
    * @date 14/07/2015
    * @version 1.0
    */
  function EnderecoSearchCtrl( GenericView, Resource, $location, $filter, pagination, $mdDialog ) {
    var vm = this;

    pagination = pagination.get('EnderecoSearchCtrl');

    //vm.order = orderFn;
    vm.onEditClick = onEditClickFn;

    vm.showAlert=showAlert;

    // Inicializa o generic: contexto, resource, modulo, id do objeto.
    GenericView.init( vm, Resource, 'endereco', 'idEndereco' );

    //carrega o conteúdo do grid
    vm.initPagination();

    //Ação do editar
    function onEditClickFn(data) {
      $location.path('endereco/edit/' + data.idObjeto);
    }

    // Ordena a lista do grid.
    function orderFn(data) {
      vm.provider = $filter('orderBy')(vm.provider, data.column, !data.desc);
    }

    function showAlert(ev) {
      $mdDialog.show({
        //controller: DialogController,
        templateUrl: 'app/modules/endereco/templates/form.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true
      });
    }
  }
});
