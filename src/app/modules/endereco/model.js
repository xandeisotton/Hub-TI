define(function() {
  'use strict';

  return {

    'idEndereco': undefined,
    'dsLogradouro': undefined,
    'dsRua': undefined,
    'dsBairro': undefined,
    'dsNumero': undefined,
    'cidade': {
      'idCidade':undefined,
    },
    'estado': {
      'idEstado':undefined
    }

  };

});
