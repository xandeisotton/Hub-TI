define(function(require) {
  'use strict';

  var angular = require('angular');
  require('angularResource');

  require('uiRouter');
  require('uiBootstrap');
  require('angularMaterial');

  // angular module definition
  return angular.module(
    // module name
    'endereco',

    // module dependencies
    [
      'ngResource',

      'ui.router',
      'ui.bootstrap',
      'ngMaterial',

      require('shared/fend/input-utils/package').name,
      require('shared/fend/pagination/package').name,

    ]
  );

});
