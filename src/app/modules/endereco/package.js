define(function(require) {
  'use strict';

  var module = require('./module');
  require('./controllers/search');
  require('./controllers/crud');
  require('./resources/rest');
  require('./states');
  require('./model');

  return module;

});
