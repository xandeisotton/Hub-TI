define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/endereco', '/endereco/list'); // default

    $stateProvider
      .state('endereco', {
        abstract: true,
        url: '/endereco',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('endereco.list', {
        url: '/list',
        views: {
          'content@endereco': {
            templateUrl   : 'app/modules/endereco/templates/list.html',
            controller    : 'EnderecoSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('endereco.new', {
        url: '/new',
        views: {
          'content@endereco': {
            templateUrl   : 'app/modules/endereco/templates/form.html',
            controller    : 'EnderecoCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('endereco.edit', {
        url: '/edit/:id',
        views: {
          'content@endereco': {
            templateUrl   : 'app/modules/endereco/templates/form.html',
            controller    : 'EnderecoCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
