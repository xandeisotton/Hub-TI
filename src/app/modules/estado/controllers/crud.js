define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('EstadoCrudCtrl', EstadoCrudCtrl);

  EstadoCrudCtrl.$inject = ['EstadoResource', '$state', 'GenericCrudService' ];

  function EstadoCrudCtrl(Resource, $state, GenericCrudService ) {

    var vm = this;

    var path = 'estado';

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Estado';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Estado';

    //Título do formulário
    vm.title = 'Estado';

    GenericCrudService.init( vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idEstado', onInit );

    function onInit() {

    }

  }
});
