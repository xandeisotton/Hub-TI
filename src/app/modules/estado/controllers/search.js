define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('EstadoSearchCtrl', EstadoSearchCtrl);

  EstadoSearchCtrl.$inject = [
    'GenericViewService', 'EstadoResource', '$location', '$filter', 'PaginationFactory' ];

  function EstadoSearchCtrl( GenericView, Resource, $location, $filter, pagination ) {
    var vm = this;

    pagination = pagination.get('EstadoSearchCtrl');

    //vm.order = orderFn;
    vm.onEditClick = onEditClickFn;

    vm.onSaveClick = onSaveClickFn;

    vm.iniciarEdicao = iniciarEdicaoFn;

    vm.cancelarEdicao = cancelarEdicaoFn;

    // Inicializa o generic: contexto, resource, modulo, id do objeto.
    GenericView.init( vm, Resource, 'estado', 'idEstado' );

    //carrega o conteúdo do grid
    vm.initPagination();

    //Ação do editar
    function onEditClickFn(data) {
      delete data.edit;
      Resource.update(data);
    }

    function onSaveClickFn(data){
      Resource.save(data, function(result){

        vm.item = generateNewItemFn();

        vm.initPagination();
      });
    }

    // Ordena a lista do grid.
    function orderFn(data) {
      vm.provider = $filter('orderBy')(vm.provider, data.column, !data.desc);
    }

    function generateNewItemFn() {
      var model = require('../model');
      return new Resource( angular.copy( model ) );
    }

    function iniciarEdicaoFn (data) {
      vm.dsEstadoOriginal = data.dsEstado;
      data.edit = true;
    }

    function cancelarEdicaoFn (data) {
      data.dsEstado = vm.dsEstadoOriginal;
      //data = vm.objetoCopy;
      data.edit = false;
    }

  }

});
