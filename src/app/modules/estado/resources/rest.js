define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('EstadoResource', EstadoResource);

  //---

  EstadoResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function EstadoResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/estado/:id', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            method : 'POST',
            url : rootUrl + '/estado/:subpath',
            params : {subpath : 'findwhen'}
        }
      }
    );

    return rest;

  }

});
