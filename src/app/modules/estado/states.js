define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/estado', '/estado/list'); // default

    $stateProvider
      .state('estado', {
        abstract: true,
        url: '/estado',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('estado.list', {
        url: '/list',
        views: {
          'content@estado': {
            templateUrl   : 'app/modules/estado/templates/list.html',
            controller    : 'EstadoSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('estado.new', {
        url: '/new',
        views: {
          'content@estado': {
            templateUrl   : 'app/modules/estado/templates/form.html',
            controller    : 'EstadoCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('estado.edit', {
        url: '/edit/:id',
        views: {
          'content@estado': {
            templateUrl   : 'app/modules/estado/templates/form.html',
            controller    : 'EstadoCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
