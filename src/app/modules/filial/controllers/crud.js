define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('FilialCrudCtrl', FilialCrudCtrl);

  FilialCrudCtrl.$inject = ['FilialResource', '$state', 'GenericCrudService', '$mdDialog' ];

  /**
  * @author Luiz Gustavo Schneider<gustavofblack12@gmail.com>
  * @date 02/09/2015
  * @version 1.0
  * */
  function FilialCrudCtrl(Resource, $state, GenericCrudService, $mdDialog ) {

    var vm = this;

    var path = 'filial';

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Filial';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Filial';

    //Título do formulário
    vm.title = 'Filiais';

    vm.empresas = [];

    GenericCrudService.init( vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idFilial', onInit );

    function onInit() {

    }

    (function selectEmpresa(){
      Resource.findPagination(function(result){
        vm.empresas = result;
      });
    })();

  }
});
