define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/filial', '/filial/list'); // default

    $stateProvider
      .state('filial', {
        abstract: true,
        url: '/filial',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('filial.list', {
        url: '/list',
        views: {
          'content@filial': {
            templateUrl   : 'app/modules/filial/templates/list.html',
            controller    : 'FilialSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('filial.new', {
        url: '/new',
        views: {
          'content@filial': {
            templateUrl   : 'app/modules/filial/templates/form.html',
            controller    : 'FilialCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('filial.edit', {
        url: '/edit/:id',
        views: {
          'content@filial': {
            templateUrl   : 'app/modules/filial/templates/form.html',
            controller    : 'FilialCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
