define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('MensagemSearchCtrl', MensagemSearchCtrl);

  MensagemSearchCtrl.$inject = [
    'GenericViewService', 'MensagemResource', '$location', '$filter', 'PaginationFactory',
        '$mdDialog'];

    /**
    * @author Luiz Gustavo Schneider<gustavofblack12@gmail.com>
    * @date 02/09/2015
    * @version 1.0
    */
  function MensagemSearchCtrl( GenericView, Resource, $location, $filter, pagination, $mdDialog ) {
    var vm = this;

    pagination = pagination.get('MensagemSearchCtrl');

    //vm.order = orderFn;
    vm.onEditClick = onEditClickFn;

    // Inicializa o generic: contexto, resource, modulo, id do objeto.
    GenericView.init( vm, Resource, 'mensagem', 'idMsgEmail' );

    //carrega o conteúdo do grid
    vm.initPagination();

    //Ação do editar
    function onEditClickFn(data) {
      $location.path('mensagem/edit/' + data.idObjeto);
    }

    // Ordena a lista do grid.
    function orderFn(data) {
      vm.provider = $filter('orderBy')(vm.provider, data.column, !data.desc);
    }


  }

});
