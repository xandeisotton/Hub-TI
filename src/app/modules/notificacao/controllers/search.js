define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('NotificacaoSearchCtrl', NotificacaoSearchCtrl);

  NotificacaoSearchCtrl.$inject = [
    'GenericViewService', 'NotificacaoResource', '$location', '$filter', 'PaginationFactory' ];

  function NotificacaoSearchCtrl( GenericView, Resource, $location, $filter, pagination ) {
    var vm = this;

    vm.editarAtividade = editarAtividadeFn;

    pagination = pagination.get('NotificacaoSearchCtrl');

    //vm.order = orderFn;
    vm.onEditClick = onEditClickFn;

    // Inicializa o generic: contexto, resource, modulo, id do objeto.
    GenericView.init( vm, Resource, 'notificacao', 'idNotificacao' );

    //carrega o conteúdo do grid
    vm.initPagination();

    //Ação do editar
    function onEditClickFn(data) {
      $location.path('notificacao/edit/' + data.idObjeto);
    }

    // Ordena a lista do grid.
    function orderFn(data) {
      vm.provider = $filter('orderBy')(vm.provider, data.column, !data.desc);
    }

  }

});
