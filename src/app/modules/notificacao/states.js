define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/notificacao', '/notificacao/list'); // default

    $stateProvider
      .state('notificacao', {
        abstract: true,
        url: '/notificacao',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('notificacao.list', {
        url: '/list',
        views: {
          'content@notificacao': {
            templateUrl   : 'app/modules/notificacao/templates/list.html',
            controller    : 'NotificacaoSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('notificacao.new', {
        url: '/new',
        views: {
          'content@notificacao': {
            templateUrl   : 'app/modules/notificacao/templates/form.html',
            controller    : 'NotificacaoCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('notificacao.edit', {
        url: '/edit/:id',
        views: {
          'content@notificacao': {
            templateUrl   : 'app/modules/notificacao/templates/form.html',
            controller    : 'NotificacaoCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('notificacao.detalhes', {
              url: '/detalhes/:id',
              views: {
                'content@notificacao': {
                  templateUrl   : 'app/modules/notificacao/templates/form.html',
                  controller    : 'NotificacaoCrudCtrl',
                  controllerAs  : 'vm'
          }
        }
      });

  }

});
