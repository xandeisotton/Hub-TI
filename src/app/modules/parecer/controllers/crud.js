define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('ParecerCrudCtrl', ParecerCrudCtrl);

  ParecerCrudCtrl.$inject = ['ParecerResource', '$state', 'GenericCrudService' ];

  function ParecerCrudCtrl(Resource, $state, GenericCrudService ) {

    var vm = this;

    var path = 'parecer';

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Parecer';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Parecer';

    //Título do formulário
    vm.title = 'Parecer';

    vm.empresas = [];

    GenericCrudService.init( vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idParecer', onInit );

    function onInit() {

    }

    function selectEmpresa(){
      Resource.findPagination(function(result){
      vm.empresas = result;
    });
    }

    selectEmpresa();

  }
});
