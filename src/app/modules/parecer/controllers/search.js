define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('ParecerSearchCtrl', ParecerSearchCtrl);

  ParecerSearchCtrl.$inject = [
    'GenericViewService', 'ParecerResource', '$location', '$filter', 'PaginationFactory', '$mdDialog' ];

  function ParecerSearchCtrl( GenericView, Resource, $location, $filter, pagination, $mdDialog ) {
    var vm = this;

    pagination = pagination.get('ParecerSearchCtrl');

    //vm.order = orderFn;
    vm.onEditClick = onEditClickFn;

    vm.showConfirm = showConfirmFn;

    // Inicializa o generic: contexto, resource, modulo, id do objeto.
    GenericView.init( vm, Resource, 'parecer', 'idParecer' );

    //carrega o conteúdo do grid
    vm.initPagination();

    //Ação do editar
    function onEditClickFn(data) {
      $location.path('parecer/edit/' + data.idObjeto);
    }

    // Ordena a lista do grid.
    function orderFn(data) {
      vm.provider = $filter('orderBy')(vm.provider, data.column, !data.desc);
    }

    function showConfirmFn(ev, parecer) {
      var confirm = $mdDialog.confirm()
          .title('Excluir')
          .content('Voce realmente deseja excluir este registro')
          .ariaLabel('Lucky day')
          .clickOutsideToClose(true)
          .targetEvent(ev)
          .ok('OK')
          .cancel('Cancelar');
      $mdDialog.show(confirm).then(function() {
        vm.onDeleteClick(parecer);
      }, function() {
      });
    }

  }

});
