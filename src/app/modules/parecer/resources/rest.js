define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('ParecerResource', ParecerResource);

  //---

  ParecerResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function ParecerResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/parecer/:idParecer', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            method : 'POST',
            url : rootUrl + '/parecer/:subpath',
            params : {subpath : 'findwhen'}
        },

        'findPagination':{
          method: 'GET', isArray:true,
          url:rootUrl + '/empresa'
        },

      }

    );

    return rest;

  }

});
