define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/parecer', '/parecer/list'); // default

    $stateProvider
      .state('parecer', {
        abstract: true,
        url: '/parecer',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('parecer.list', {
        url: '/list',
        views: {
          'content@parecer': {
            templateUrl   : 'app/modules/parecer/templates/list.html',
            controller    : 'ParecerSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('parecer.new', {
        url: '/new',
        views: {
          'content@parecer': {
            templateUrl   : 'app/modules/parecer/templates/form.html',
            controller    : 'ParecerCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('parecer.edit', {
        url: '/edit/:id',
        views: {
          'content@parecer': {
            templateUrl   : 'app/modules/parecer/templates/form.html',
            controller    : 'ParecerCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
