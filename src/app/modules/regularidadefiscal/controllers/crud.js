define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('RegularidadeFiscalCrudCtrl', RegularidadeFiscalCrudCtrl);

  RegularidadeFiscalCrudCtrl.$inject = ['RegularidadeFiscalResource', '$state', 'GenericCrudService' ];

  function RegularidadeFiscalCrudCtrl(Resource, $state, GenericCrudService ) {

    var vm = this;

    var path = 'regularidadeFiscal';

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Regularidade Fiscal';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Regularidade Fiscal';

    //Título do formulário
    vm.title = 'Regularidade Fiscal';

    vm.empresas = [];

    GenericCrudService.init( vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idRegularidadeFiscal', onInit );

    function onInit() {

    }

    function selectEmpresa(){
      Resource.findPagination(function(result){
        vm.empresas = result;
      });
    }

  selectEmpresa();

  }
});
