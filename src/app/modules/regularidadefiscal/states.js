define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/regularidadefiscal', '/regularidadefiscal/list'); // default

    $stateProvider
      .state('regularidadefiscal', {
        abstract: true,
        url: '/regularidadefiscal',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('regularidadefiscal.list', {
        url: '/list',
        views: {
          'content@regularidadefiscal': {
            templateUrl   : 'app/modules/regularidadefiscal/templates/list.html',
            controller    : 'RegularidadeFiscalSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('regularidadefiscal.new', {
        url: '/new',
        views: {
          'content@regularidadefiscal': {
            templateUrl   : 'app/modules/regularidadefiscal/templates/form.html',
            controller    : 'RegularidadeFiscalCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('regularidadefiscal.edit', {
        url: '/edit/:id',
        views: {
          'content@regularidadefiscal': {
            templateUrl   : 'app/modules/regularidadefiscal/templates/form.html',
            controller    : 'RegularidadeFiscalCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
