define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('SocioCrudCtrl', SocioCrudCtrl);

  SocioCrudCtrl.$inject = ['SocioResource', '$state', 'GenericCrudService' ];

  /**
  * @author Luiz Gustavo Schneider<gustavofblack12@gmail.com  >
  * @date 01/09/2015
  * @version 1.0
  * */
  function SocioCrudCtrl(Resource, $state, GenericCrudService ) {

    var vm = this;

    var path = 'socio';

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Sócios';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Sócios';

    //Título do formulário
    vm.title = 'Sócios';

    vm.empresas = [];

    GenericCrudService.init( vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idSocio', onInit );

    function onInit() {

    }

    function selectEmpresa(){
      Resource.findPagination(function(result){
      vm.empresas = result;
    });
    }

    selectEmpresa();

  }
});
