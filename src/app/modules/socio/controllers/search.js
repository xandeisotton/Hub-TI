define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('SocioSearchCtrl', SocioSearchCtrl);

  SocioSearchCtrl.$inject = [
    'GenericViewService', 'SocioResource', '$location', '$filter', 'PaginationFactory', '$mdDialog'];

    /**
    * @author Luiz Gustavo Schneider<gustavofblack12@gmail.com>
    * @date 01/09/2015
    * @version 1.0
    */
  function SocioSearchCtrl( GenericView, Resource, $location, $filter, pagination, $mdDialog) {
    var vm = this;

    pagination = pagination.get('SocioSearchCtrl');

    //vm.order = orderFn;
    vm.onEditClick = onEditClickFn;

    vm.showConfirm = showConfirmFn;

    // Inicializa o generic: contexto, resource, modulo, id do objeto.
    GenericView.init( vm, Resource, 'socio', 'idSocio' );

    //carrega o conteúdo do grid
    vm.initPagination();

    //Ação do editar
    function onEditClickFn(data) {
      $location.path('socio/edit/' + data.idObjeto);
    }

    // Ordena a lista do grid.
    function orderFn(data) {
      vm.provider = $filter('orderBy')(vm.provider, data.column, !data.desc);
    }

      function showConfirmFn(ev, socio) {
        var confirm = $mdDialog.confirm()
            .title('Excluir')
            .content('Voce realmente deseja excluir este registro')
            .ariaLabel('Lucky day')
            .clickOutsideToClose(true)
            .targetEvent(ev)
            .ok('OK')
            .cancel('Cancelar');
        $mdDialog.show(confirm).then(function() {
          vm.onDeleteClick(socio);
        }, function() {
        });
      }

  }

});
