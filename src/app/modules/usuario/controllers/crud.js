define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('UsuarioCrudCtrl', UsuarioCrudCtrl);

  UsuarioCrudCtrl.$inject = ['UsuarioResource', '$state', 'GenericCrudService', '$mdDialog'];


  function UsuarioCrudCtrl(Resource, $state, GenericCrudService, $mdDialog) {

    var vm = this;

    var path = 'usuario';

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Usuario';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Usuario';

    //Título do formulário
    vm.title = 'Usuario';

    GenericCrudService.init(vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idUsuario', onInit);

    function onInit() {

    }

  }

});
