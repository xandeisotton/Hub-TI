define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('UsuarioSearchCtrl', UsuarioSearchCtrl);

  UsuarioSearchCtrl.$inject = [
    'GenericViewService', 'UsuarioResource', '$location', '$filter', 'PaginationFactory'];

    /**
    * @author Luiz Gustavo Schneider <gustavofblack12@gmail.com>
    * @date 22/09/2015
    * @version 1.0
    */
  function UsuarioSearchCtrl( GenericView, Resource, $location, $filter, pagination){
    var vm = this;

    pagination = pagination.get('UsuarioSearchCtrl');

    //vm.order = orderFn;
    vm.onEditClick = onEditClickFn;

    // Inicializa o generic: contexto, resource, modulo, id do objeto.
    GenericView.init( vm, Resource, 'usuario', 'idUsuario');

    //carrega o conteúdo do grid
    vm.initPagination();

    //Ação do editar
    function onEditClickFn(data) {
      $location.path('usuario/edit/' + data.idObjeto);
    }

    // Ordena a lista do grid.
    function orderFn(data) {
      vm.provider = $filter('orderBy')(vm.provider, data.column, !data.desc);
    }

  }
});
