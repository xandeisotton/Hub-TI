define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('UsuarioResource', UsuarioResource);

  //---

  UsuarioResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function UsuarioResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/usuario/:id', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            method : 'POST',
            url : rootUrl + '/usuario/:subpath',
            params : {subpath : 'findwhen'}
        }
      }
    );

    return rest;

  }

});
